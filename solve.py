class CharacterPiece:
    def __init__(self, character, part):
        self.part = part
        self.character = character

    def __repr__(self):
        return "{} {}".format(self.character, self.part)

    def __eq__(self, other):
        return other.part == self.part and other.character == self.character

    def compatible_with(self, p):
        return p in self.character.pieces() and p != self


class Character:
    def __init__(self, description):
        self.description = description
        self.front = CharacterPiece(self, "Front")
        self.back = CharacterPiece(self, "Back")

    def __repr__(self):
        return self.description

    def __eq__(self, other):
        return self.description == other.description

    def pieces(self):
        return [self.front, self.back]


class Tile:
    def __init__(self, id, edges):
        self.edges = edges
        self.id = id

    def edge(self, direction):
        try:
            return self.edges[direction]
        except KeyError:
            return None

    def __repr__(self):
        return "Tile({},{})".format(self.id, self.edges)

    def __eq__(self, other):
        return self.id == other.id


NoTile = Tile(None, {})


class Solution:

    def __init__(self, space, parent_solution=None, location=None, tile=None):
        self.space = space
        if parent_solution is not None:
            self.placed_tiles = parent_solution.placed_tiles.copy()
        else:
            self.placed_tiles = {}
        if location is not None and tile is not None:
            self.placed_tiles[location] = tile

    def is_complete(self):
        return len(self.empty_spaces()) == 0

    def empty_spaces(self):
        return [location for location in self.space.locations if location not in self.placed_tiles.keys()]

    def unused_tiles(self):
        return (t for t in tiles if t not in self.placed_tiles.values())

    def tile_at(self, location):
        if location in self.placed_tiles:
            return self.placed_tiles[location]
        else:
            return NoTile

    def is_tile_valid(self, location):
        tile = self.tile_at(location)
        if tile is NoTile:
            return True
        for direction in self.space.directions:
            neighbour = self.tile_at(self.space.adjust(location, direction))
            if not self._edges_compatible(tile.edge(direction), neighbour.edge(self.space.opposite(direction))):
                return False
        return True

    def _edges_compatible(self, a, b):
        return a is None or b is None or a.compatible_with(b)

    def is_valid_partial(self):
        for location in self.space.locations:
            if not self.is_tile_valid(location):
                return False
        return True

    def __repr__(self):
        str = ""
        for location in self.space.locations:
            str += "{} = {}\n".format(location, self.tile_at(location))
        return str


class Cartesian2DSpace:
    north = (0, -1)
    east = (1, 0)
    south = (0, 1)
    west = (-1, 0)

    directions = [north, east, south, west]

    def __init__(self, width, height):
        self.locations = [(x, y) for x in range(0, width) for y in range(0, height)]

    def adjust(self, location, direction):
        x, y = location
        dx, dy = direction
        return x + dx, y + dy

    def opposite(self, direction):
        dx, dy = direction
        return -1 * dx, -1 * dy

    def rotate(self, direction):
        dx, dy = direction
        return dy, -1 * dx

    def variants(self, tile):
        a = tile
        for _ in self.directions:
            yield a
            a = self.rotated(a)

    def rotated(self,tile):
        return Tile(tile.id, {self.rotate(k): v for k, v in tile.edges.items()})

class Solver:
    def more_complete_solutions(self, current_solution, recurse=False):
        if not current_solution.is_complete():
            target_space = current_solution.empty_spaces()[0]
            tile_varients_to_try = (t for tile in current_solution.unused_tiles() for t in current_solution.space.variants(tile))
            candidate_solutions = (Solution(current_solution.space, current_solution, target_space, tile) for tile in tile_varients_to_try)
            valid_solutions = (solution for solution in candidate_solutions if solution.is_valid_partial())
            for solution in valid_solutions:
                yield solution
                if recurse:
                    for subs in self.more_complete_solutions(solution, recurse=recurse):
                        yield subs

    def find_complete_solutions(self, starting_solution):
        valid_solutions = solver.more_complete_solutions(initial_solution, recurse=True)
        return (solution for solution in valid_solutions if solution.is_complete())

space = Cartesian2DSpace(3, 3)

dg = Character("Dark Green")
y = Character("Yellow")
lg = Character("Light Green")
g = Character("Grey")

tiles = [
    Tile(1, {space.north: dg.front, space.east: lg.back, space.south: y.back, space.west: g.back}),
    Tile(2, {space.north: dg.front, space.east: g.front, space.south: y.front, space.west: lg.front}),
    Tile(3, {space.north: y.front, space.east: g.front, space.south: dg.back, space.west: lg.front}),
    Tile(4, {space.north: g.back, space.east: y.back, space.south: lg.back, space.west: dg.front}),
    Tile(5, {space.north: g.front, space.east: dg.front, space.south: g.back, space.west: lg.back}),
    Tile(6, {space.north: dg.front, space.east: dg.back, space.south: lg.back, space.west: y.back}),
    Tile(7, {space.north: dg.front, space.east: g.back, space.south: y.front, space.west: lg.back}),
    Tile(8, {space.north: dg.back, space.east: y.back, space.south: lg.back, space.west: g.front}),
    Tile(9, {space.north: lg.front, space.east: g.back, space.south: y.front, space.west: y.front})
]

if __name__ == "__main__":
    solver = Solver()
    initial_solution = Solution(space)
    for solution in solver.find_complete_solutions(initial_solution):
        print(solution)
